//
//  NetworkService.swift
//  DesignPattern
//
//  Created by Nalinda Wickramarathna on 3/16/22.
//

import Foundation

final class NetworkService {
    static let shared = NetworkService()
    
    private var user: User?
    private let postViewModel = PostListViewModel()
    
    private init() { }
    
    /**
     Login service
     */
    func login(email: String, password: String, completion: @escaping(Bool) -> Void) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            if email == "test@test.com" && password == "password" {
                self?.user = User(firstName: "Nalinda", lastName: "Wick",
                            email: "test@test.com", age: 30)
                completion(true)
            } else {
                self?.user = nil
                completion(false)
            }
        }
    }
    
    /**
     Get already logged in user
     */
    func getLoggedInUser() -> User {
        return user!
    }
    
    func fetchPosts(completion: @escaping([Post]) -> Void) {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else { return }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, _ in
            guard let data = data else { return }
            do {
                let posts = try JSONDecoder().decode([Post].self, from: data)
                completion(posts)
            } catch {

            }
        }
        task.resume()
    }
}
