//
//  Post.swift
//  DesignPattern
//
//  Created by Nalinda Wickramarathna on 3/26/22.
//

import Foundation

struct Post: Codable {
    let title: String
}
