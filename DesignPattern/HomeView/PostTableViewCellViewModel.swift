//
//  UserTableViewCellViewModel.swift
//  DesignPattern
//
//  Created by Nalinda Wickramarathna on 3/26/22.
//

import Foundation

struct PostTableViewCellViewModel {
    let title: String
}
