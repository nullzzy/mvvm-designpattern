//
//  UserListViewModel.swift
//  DesignPattern
//
//  Created by Nalinda Wickramarathna on 3/26/22.
//

import Foundation

class PostListViewModel {
    var posts: ObservableObject<[PostTableViewCellViewModel]> = ObservableObject([])
    
    func fetchPosts() {
        NetworkService.shared.fetchPosts { [weak self] newposts in
            self?.posts.value = newposts.compactMap({
                PostTableViewCellViewModel(title: $0.title)
            })
        }
    }
}
